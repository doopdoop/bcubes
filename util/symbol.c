/*
 * Code for a monotonically growing symbol table.
 * FIXME :
 *         - you can't delete symbols in a table.
 *         - searching is really cruddy linear-time.
 *         - symbols can't point directly to other symbols,
 *           since realloc() is called on the tables.
 *
 * The optimal way of handling the above is probably to redo
 * the table structure as an ordered tree. Hashes for balancing?
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "symbol.h"


/* Create a new symbol with the given name and data.
 * Return a pointer to the symbol on success;
 * NULL indicates failure.
 */

symbol_t *symbol_new(char *name, datatype_t type, symdata_t data )
{
	symbol_t *symbol;
	char *newname; /* Symbol's copy of its name */
	size_t len; /* Length of name, including null */

	len=1+strlen(name);
	if (NULL==(newname=malloc(len))) {
		perror("symbol_new(): malloc()");
		return NULL;
	}
	strncpy(newname,name,len);

	if (NULL==(symbol=malloc(sizeof(symbol_t)))) {
		perror("symbol_new(): malloc()");
		free(newname);
		return NULL;
	}

	symbol->sym_name=newname;
	symbol->sym_type=type;
	symbol->sym_data=data;
	return symbol;
}

/* Deallocate a symbol and its name, but leave the pointer alone.
 * Return zero on success, nonzero on error.
 */

int symbol_destroy(symbol_t *sym)
{
	/* Unfortunately, free(3) has no return value,
	 * so we can't do much error checking.
	 */

	free(sym->sym_name);
	free(sym);
	return 0;
}

/* Deallocate a symbol and its name.
 * Also, if its data is of a pointer type (eg, a string, rather than
 * an embedded int or double, etc), then that is freed as well.
 * Return zero on success, nonzero on error.
 */

int symbol_destroy_all(symbol_t *sym)
{
	if (TYPE_STRING==sym->sym_type) {
		if (NULL!=sym->sym_data.dat_str) {
			free(sym->sym_data.dat_str);
		}
	}
	free(sym->sym_name);
	free(sym);
	return 0;
}

int symbol_isstring(symbol_t *sym) {
	return TYPE_STRING==sym->sym_type;
}
int symbol_isnumeric(symbol_t *sym) {
	return (TYPE_INTEGER==sym->sym_type) ||
		(TYPE_DOUBLE==sym->sym_type) ;
}
int symbol_isinteger(symbol_t *sym) {
	return TYPE_INTEGER==sym->sym_type;
}
int symbol_isdouble(symbol_t *sym) {
	return TYPE_DOUBLE==sym->sym_type;
}

/*
 * Search for a symbol with the given name:
 * if it's found and a double or integer, write this value into
 * the pointer d, and return 0.
 * If it's found and not a double or integer, write an error message
 * to stderr and return 1.
 * If it's not found, return 0.
 * Other return values indicate error.
 */
int symbol_setdouble(symtab_t *symtab,char *name,double *d)
{
	
	symbol_t *symbol;

	if (NULL==(symbol=symtab_lookup(symtab,name))) {
		return 0;
	}
	if (symbol_isinteger(symbol)) {
		*d=(double) symbol->sym_data.dat_int;
		return 0;
	} else if (symbol_isdouble(symbol)) {
		*d=symbol->sym_data.dat_dou;
		return 0;
	} else {
		fprintf(stderr,
			"Error: non-numeric type for double symbol \"%s\"\n",
			name);
		return 1;
	}

}

/*
 * Search for a symbol with the given name:
 * If it's found and aninteger, write this value into
 * the pointer d, and return 0.
 * If it's found and not an integer, write an error message
 * to stderr and return 1.
 * If it's not found, return 0.
 * Other return values indicate error.
 */

int symbol_setinteger(symtab_t *symtab,char *name,int *d)
{
	
	symbol_t *symbol;

	if (NULL==(symbol=symtab_lookup(symtab,name))) {
		return 0;
	}
	if (symbol_isinteger(symbol)) {
		*d= symbol->sym_data.dat_int;
		return 0;
	} else {
		fprintf(stderr,
			"Error: non-integer type for integer symbol \"%s\"\n",
			name);
		return 1;
	}

}

/*
 * Search for a symbol with the given name:
 * If it's found and a string, write the string pointer into
 * the pointer d, and return 0.
 * If it's found and not a string, write an error message
 * to stderr and return 1.
 * If it's not found, return 0.
 * Other return values indicate error.
 */

int symbol_setstring(symtab_t *symtab,char *name,char **d)
{
	
	symbol_t *symbol;

	if (NULL==(symbol=symtab_lookup(symtab,name))) {
		return 0;
	}
	if (symbol_isstring(symbol)) {
		*d= symbol->sym_data.dat_str;
		return 0;
	} else {
		fprintf(stderr,
			"Error: non-string type for string symbol \"%s\"\n",
			name);
		return 1;
	}

}

/*
 * Search for a symbol with the given name:
 * If it's found and a string:
 *	If it equals "yes", "y", "t", "true" (case-insensitive),
 *		set the given integer to 1; return 0.
 *	If it equals "no","n","f","false" (case-insensitive),
 *		set the given integer to 0; return 0.
 *	Otherwise, write an error message to stderr and return 1.
 * If it's found and an integer:
 *	If it equals zero, set the given integer to 0; return 0.
 *	Otherwise, set the given integer to 1; return 0.
 * If it's neither a string nor an integer, write an error message
 * to stderr and return 1.
 * Other return values indicate error.
 */

int symbol_setbool(symtab_t *symtab,char *name,int *i)
{
	
	symbol_t *symbol;
	char *s;
	int j;

	if (NULL==(symbol=symtab_lookup(symtab,name))) {
		return 0;
	}
	if (symbol_isstring(symbol)) {
		s= symbol->sym_data.dat_str;
		if (	(0==strcasecmp("y",s)) ||
			(0==strcasecmp("yes",s)) ||
			(0==strcasecmp("t",s)) ||
			(0==strcasecmp("true",s))
		) {
			*i=1;
			return 0;
		} else if (	(0==strcasecmp("n",s)) ||
				(0==strcasecmp("no",s)) ||
				(0==strcasecmp("f",s)) ||
				(0==strcasecmp("false",s))
		) {
			*i=0;
			return 0;
		} else {
			fprintf(stderr,
				"Error: non-Boolean value for symbol \"%s\"\n",
				name);
			return 1;
		}

	} else if (symbol_isinteger(symbol)) {
		j= symbol->sym_data.dat_int;
		if (0==j) {
			*i=0;
		} else {
			*i=1;
		}
		return 0;
	} else {
		fprintf(stderr,
			"Error: non-Boolean type for symbol \"%s\"\n",
			name);
		return 1;
	}

}

/*
 * Allocate a symbol table to hold size symbols.
 * Return pointer to table on success, NULL on error.
 */
symtab_t *symtab_new(unsigned int size)
{
	symbol_t *table;	/* Array containing symbols */
	symtab_t *symtab;

	if (NULL==(table=malloc(sizeof(symbol_t)*size))) {
		perror("symtab_new(): malloc()");
		return NULL;
	}
	if (NULL==(symtab=malloc(sizeof(symtab_t)))) {
		perror("symtab_new(): malloc()");
		free(table);
		return NULL;
	}

	symtab->tab_size=size;
	symtab->tab_no=0;
	symtab->tab_data=table;
	return symtab;
}

/*
 * Destroy a symbol table, INCLUDING the symbols contained within,
 * but NOT including the symbols' pointer data.
 * Return 0 on success, nonzero on error.
 */

int symtab_destroy(symtab_t *symtab)
{
	free(symtab->tab_data);
	free(symtab);
	return 0;
}

/*
 * Destroy a symbol table, INCLUDING the symbols contained within,
 * AND the symbols' pointer data.
 * Return 0 on success, nonzero on error.
 */

int symtab_destroy_all(symtab_t *symtab)
{
	unsigned int i;

	for (i=0;i<symtab->tab_no;i++) {
		if (TYPE_STRING==symtab->tab_data[i].sym_type) {
			free(symtab->tab_data[i].sym_data.dat_str);
		}
	}

	free(symtab->tab_data);
	free(symtab);
	return 0;
}

/*
 * Look for a symbol with the given name in the given symbol table.
 * Return a pointer to the symbol if it's found, otherwise NULL.
 * NB: This does a linear-time search; it could easily be optimized.
 */

symbol_t *symtab_lookup(symtab_t *symtab, char *name)
{
	unsigned int i=0;

	for (i=0;i<symtab->tab_no;i++) {
		if (0==strcmp(name,symtab->tab_data[i].sym_name)) {
			return symtab->tab_data+i;
		}
	}
	return NULL;
}

/*
 * Create a new symbol with the given name and data in the given
 * symbol table. If a symbol with the same name already exists,
 * no change is made to the symbol table, and 1 is returned.
 * The symbol table will be expanded (if possible), if there is
 * insufficient space to store the new symbol.
 * Return zero on success, nonzero on error.
 */

int symtab_add(symtab_t *symtab, char *name, datatype_t type, symdata_t data)
{
	symbol_t *newptr=NULL;
	size_t newsize;

#ifdef SYMBOL_DEBUG
	fprintf(stderr,"symtab_add(%p,\"%s\",%d)\n",(void *)symtab,name,type);
#endif

	if (NULL!=symtab_lookup(symtab,name)) {
		/* Symbol already exists */
		return 1;
	}
	if (symtab->tab_no > symtab->tab_size) {
		/* Corrupt symbol table -- abort. */
		fprintf(stderr,
		"DANGER: symtab_add(): table of size %u has %u entries!\n",
			symtab->tab_size,symtab->tab_no);
		fprintf(stderr,"symtab_add(): aborting.\n");
		return -1;
	}

	if (symtab->tab_no == symtab->tab_size ) {
		/* We need to allocate more space */

		/* First, try twice the current table size */
		newsize=2*symtab->tab_size; 

		while (	(newsize > symtab->tab_size) ) {

			newptr=realloc(symtab->tab_data,
					newsize*sizeof(symbol_t));

			if (NULL!=newptr) {
				break;
			} 
			/* realloc() failed; try something smaller. */
			newsize--;
		}

		if (NULL==newptr) {
			perror("symtab_add(): realloc()");
			return -1;
		}

		symtab->tab_data=newptr;
		symtab->tab_size=newsize;

		/* New table has been allocated and filled in by realloc() */
	}

	/* At this point, the table has space for at least one
	 * more symbol.
	 */

	symtab->tab_data[symtab->tab_no].sym_name=name;
	symtab->tab_data[symtab->tab_no].sym_type=type;
	symtab->tab_data[symtab->tab_no].sym_data=data;

	symtab->tab_no++;

	return 0;
}
/*
 * Print names of all symbols in the given table, and their values,
 * to the given file.
 * Return value as yet undefined.
 */
int symtab_dump_to_fd(symtab_t *symtab, FILE *outfile)
{
	unsigned int i;

	fprintf(outfile,"Dumping table of %u symbols (%u max):\n",
		symtab->tab_no,symtab->tab_size);

	for (i=0;i<symtab->tab_no;i++) {
		fprintf(outfile,"%u: \"%s\" ",i,symtab->tab_data[i].sym_name);
		switch(symtab->tab_data[i].sym_type) {
			case TYPE_INTEGER:
				fprintf(outfile,"int=%d\n",
					symtab->tab_data[i].sym_data.dat_int);
				break;
			case TYPE_DOUBLE:
				fprintf(outfile,"dub=%f\n",
					symtab->tab_data[i].sym_data.dat_dou);
				break;
			case TYPE_STRING:
				fprintf(outfile,"str=\"%s\"\n",
					symtab->tab_data[i].sym_data.dat_str);
				break;
			default:
				fprintf(outfile,"Unknown type\n");
				break;
		}
	}
	return 0;
}
/* As above, but to stdout */
int symtab_dump(symtab_t *symtab)
{
	return symtab_dump_to_fd(symtab,stdout);
}


/*
 * free() all pointer data in the defined symbols, and reset the table
 * to contain no symbols.
 * 
 * Useful if you already have a symbol table and want to wipe it without
 * freeing it and allocating a new one.
 *
 * Return zero on success, nonzero on error.
 */

int symtab_purge(symtab_t *symtab)
{
	unsigned int i;

	for (i=0;i<symtab->tab_no;i++) {
		if (TYPE_STRING==symtab->tab_data[i].sym_type) {
			if (NULL!=symtab->tab_data[i].sym_data.dat_str) {
			free(symtab->tab_data[i].sym_data.dat_str);
			}
		}
	}

	symtab->tab_no=0;
	return 0;

}

/*
 * Clear table of symbols.
 *
 * Useful if you already have a symbol table and want to wipe it without
 * freeing it and allocating a new one.
 *
 * Return zero on success, nonzero on error.
 */

int symtab_reset(symtab_t *symtab)
{
	symtab->tab_no=0;
	return 0;
}


/* Print a string, escaping as required. */

int symtab_print_string(char *s)
{

	while (0!=*s) {
		switch(*s) {
			case '\n':
				printf("\\n");
				break;
			case '\\':
				printf("\\\\");
				break;
			case '\t':
				printf("\\t");
				break;
			case '\r':
				printf("\\r");
				break;
			case '"':
				printf("\\\"");
				break;
			default:
				printf("%c",*s);
				break;
		}
		s++;
	}
	return 0;
}

/*
 * Dump all symbol values in parseable format.
 */
int symtab_dump_parseable_to_fd(symtab_t *symtab,FILE *outfile)
{
	unsigned int i;

	fprintf(outfile,"{\n");

	for (i=0;i<symtab->tab_no;i++) {
		fprintf(outfile,"\t%s = ",symtab->tab_data[i].sym_name);
		switch(symtab->tab_data[i].sym_type) {
			case TYPE_INTEGER:
				fprintf(outfile,"%d;\n",
					symtab->tab_data[i].sym_data.dat_int);
				break;
			case TYPE_DOUBLE:
				fprintf(outfile,"%f;\n",
					symtab->tab_data[i].sym_data.dat_dou);
				break;
			case TYPE_STRING:
				fprintf(outfile,"\"%s\";\n",
					symtab->tab_data[i].sym_data.dat_str);
				break;
			default:
				fprintf(outfile,"\"? Unknown symbol type ?\"\n");
				break;
		}
	}
	fprintf(outfile,"}\n");

	return 0;
}
int symtab_dump_parseable(symtab_t *symtab)
{
	return symtab_dump_parseable_to_fd(symtab,stdout);
}
