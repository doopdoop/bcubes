struct buffer{
	char *str; /* Dynamically allocated character buffer */
	int maxsize; /* Maximum size before reallocation required */
} ;

typedef struct buffer buffer_t;

buffer_t *buffer_new(int);
int buffer_destroy(buffer_t *);
int buffer_append_string(buffer_t *, char *);
int buffer_file_readline(buffer_t *, FILE *);
int buffer_test(void);
int buffer_inflate(buffer_t *, int);
int buffer_file_readtoeof(buffer_t *, FILE *);
