#ifndef INCLUDED_CBUF_H
#define INCLUDED_CBUF_H

#define CBUFTYPE char
#undef CBUFDEBUG	/* Define for debugging output */
#undef CBUFMAIN		/* Define to define main() in cbuf.c for testing */

enum cbufgrowth {
	CBUF_NONGROWABLE=0,
	CBUF_GROWABLE=1
};

typedef enum cbufgrowth cbufgrowth_t ;

struct cbuf {
	CBUFTYPE *buf; /* Dynamically allocated character buffer */
	unsigned int maxsize;
	cbufgrowth_t growth;
	unsigned int start;	/* Index of first character to be read out */
	unsigned int size;	/* No of characters in buffer */
};

typedef struct cbuf cbuf_t;


cbuf_t *cbufNew(int, cbufgrowth_t);
int cbufDestroy(cbuf_t *);
unsigned int cbufGetSize(cbuf_t *);
void cbufWipe(cbuf_t *);
int cbufResize(cbuf_t *, unsigned int );
int cbufGrow(cbuf_t *);
int cbufWrite(cbuf_t *, CBUFTYPE);
int cbufRead(cbuf_t *);
void cbufDump(cbuf_t *);
char cbufRandomChar(void);
int cbufTest(void);
#endif /* INCLUDED_CBUF_H */
