#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <png.h>
#include "cbuf.h"
#include "glutil.h"

/*************** Globals for text handling code *************/

const int termcols=TERMWIDTH;
const int termlines=TERMLINES;
const int termtabsize=8;
const int charwidth=10;
const int charheight=20;
int texscreenx=0,texscreeny=0;

image_t *cmapimg=NULL;
GLuint cmaptex;     /* Character map texture */
char *termline[TERMLINES];
int termx=0,termy=0;

#undef DIE_ON_UNKNOWN
#undef TERMDEBUG

/********************* Image handling code ************************/

/* Load a PNG into an image structure. Return a pointer
 * to the image struct on success, or NULL on error.
 */

struct image *image_frompng(char *fname) 
{
    FILE *f;
#define PNGMAGICSIZE 4 /* Size of PNG magic number */
    char pngheader[PNGMAGICSIZE];

    /* PNG metadata */
    png_structp png_ptr;
    png_infop infoptr;
    png_uint_32 pngwidth,pngheight;
    int bpp,coltype,intertype,comptype,filtype;
    int png_transforms = PNG_TRANSFORM_IDENTITY;
    image_t *image;
    unsigned char *p;
    png_bytep *rowptr;
    png_bytep q;
    int i,j;

    /* Open the png file */
    if (NULL==(f=fopen(fname,"rb"))) {
        perror("fopen()");
        return NULL;
    }

    /* Check the magic number to make sure it's a PNG */

    if (PNGMAGICSIZE != fread(pngheader, 1, PNGMAGICSIZE,f)) {
        perror("fread()");
        return NULL;
    }
    if (0!=png_sig_cmp((png_bytep)pngheader,(png_size_t)0,PNGMAGICSIZE)) {
        fprintf(stderr,"Bad magic number.\n");
        return NULL;
    }

    /* Create the PNG structure to hold state for libpng */

    if (NULL==(png_ptr=png_create_read_struct(
        PNG_LIBPNG_VER_STRING,
        NULL,NULL,NULL))) {
            fclose(f);
            fprintf(stderr,"png_create_read_struct() failed\n");
            return NULL;
    }

    /* Allocate structure for image information */

    if (NULL==(infoptr = png_create_info_struct(png_ptr))) {
        fclose(f);
        png_destroy_read_struct(&png_ptr,(png_infopp)NULL,
            (png_infopp)NULL);
        fprintf(stderr,"png_create_info_struct() failed.\n");
        return NULL;
    }

    /* Set up PNG error handling. God, this library is overengineered. */

    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr,
            (png_infopp)NULL,(png_infopp)NULL);
        fclose(f);
        fprintf(stderr,"Unable to install PNG error handler\n");
        return NULL;
    }

    /* Set up PNG I/O handling */

    png_init_io(png_ptr,f);

    /* Tell it we've already checked the signature */
    png_set_sig_bytes(png_ptr,PNGMAGICSIZE);

    /* Read the goddamned image already */

    png_read_png(png_ptr,infoptr,png_transforms,NULL);

    /* Fetch image header data */

    png_get_IHDR(png_ptr,infoptr, &pngwidth,&pngheight,
        &bpp,&coltype,&intertype,&comptype,&filtype);
    printf("PNG is %ux%u at %d bpp.\n",
        pngwidth,pngheight,bpp);

    if (8!=bpp) {
        fprintf(stderr,"Error: require 8bpp\n");
        png_destroy_read_struct(&png_ptr,&infoptr,(png_infopp)NULL);
        fclose(f);
        return NULL;
    }


    /* Get image data */

    rowptr = png_get_rows(png_ptr,infoptr);

    /* Set up my own image structure */

    if (NULL==(image=malloc(sizeof(image_t)))) {
        perror("malloc()");
        png_destroy_read_struct(&png_ptr,&infoptr,(png_infopp)NULL);
        fclose(f);
        return NULL;
    }
    switch(coltype) {
        case PNG_COLOR_TYPE_RGB:
            printf("coltype RGB\n");
            image->channels=3;
            break;
        case PNG_COLOR_TYPE_RGBA:
            printf("coltype RGBA\n");
            image->channels=4;
            break;
        case PNG_COLOR_TYPE_GRAY:
            printf("coltype GRAY\n");
            image->channels=1;
            break;
        case PNG_COLOR_TYPE_GRAY_ALPHA:
            printf("coltype GRAY_ALPHA\n");
            image->channels=2;
            break;
        default:
            fprintf(stderr,"Error: require 8bpp RGB[A]\n");
            free(image);
            png_destroy_read_struct(
                &png_ptr,&infoptr,(png_infopp)NULL);
            fclose(f);
            return NULL;
    }

    image->bpp = bpp;
    image->width = (int) pngwidth;
    image->height = (int) pngheight;
    if (NULL==(image->data=malloc(
        image->width*image->height*(image->channels)))) {
        perror("malloc()");
        png_destroy_read_struct(&png_ptr,&infoptr,(png_infopp)NULL);
        free(image);
        fclose(f);
        return NULL;
    }

    /* Transfer image from png to my buffer */

    /* FIXME: can't remember if this is the right way round, since
     * GL's Y-axis is upside down.
     */

    p=image->data;
    if (1==image->channels) {
        for (i=0;i<image->height;i++) {
            q=rowptr[i];
            for (j=0;j<image->width;j++) {
                *p++ = *q++;
            }
        }
    } else if (2==image->channels) {
        printf("LA\n");
        for (i=0;i<image->height;i++) {
            q=rowptr[i];
            for (j=0;j<image->width;j++) {
                *p++ = *q++;
                *p++ = *q++;
            }
        }
    } else if (3==image->channels) {
        printf("RGB\n");
        for (i=0;i<image->height;i++) {
            q=rowptr[i];
            for (j=0;j<image->width;j++) {
                *p++ = *q++;
                *p++ = *q++;
                *p++ = *q++;
            }
        }
    } else {
        /* Assume 4==channels */
        printf("RGBA\n");
        for (i=0;i<image->height;i++) {
            q=rowptr[i];
            for (j=0;j<image->width;j++) {
                *p++ = *q++;
                *p++ = *q++;
                *p++ = *q++;
                *p++ = *q++;
            }
        }
    }

    /* Clean up */

    png_destroy_read_struct(&png_ptr,&infoptr,(png_infopp)NULL);
    fclose(f);
    return image;
}

int image_destroy(image_t *img)
{
    free(img->data);
    free(img);
    return 0;
}

/* Return GL type corresponding to image */
GLenum imagegltype(image_t *img)
{

    switch(img->channels) {
        case 1:
            return GL_LUMINANCE;
        case 2:
            return GL_LUMINANCE_ALPHA;
        case 3:
            return GL_RGB;
        case 4:
            return GL_RGBA;
        default:
            fprintf(stderr,
            "imagegltype(): unknown type for %d channels\n",
            img->channels);
            exit(-1);
            return -1;
    }
}

int texture_fromimage(image_t *img, GLuint *tex,GLint magfilt,GLint minfilt)
{
    glGenTextures(1,tex);

    glBindTexture(GL_TEXTURE_2D,*tex);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,magfilt);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,minfilt);

    glTexImage2D(GL_TEXTURE_2D,
        0, /* Level of detail */
        img->channels,
        img->width,
        img->height,
        0, /* border */
        imagegltype(img),
        GL_UNSIGNED_BYTE,
        img->data);

    return 0;
}

image_t *texture_frompng(char *fname, GLuint *tex,GLint magfilt,GLint minfilt)
{
    image_t *img=NULL;

    if (NULL==(img=image_frompng(fname))) {
        fprintf(stderr,"texture_frompng(): Failed to load \"%s\"\n",
            fname);
        return NULL;
    }
    if (0!=texture_fromimage(img,tex,magfilt,minfilt)) {
        fprintf(stderr,"texture_frompng(): "
            "failed to generate texture from \"%s\"\n",
            fname);
        image_destroy(img);
        return NULL;
    }
    return img;
}

/************************ Text handling code ***************************/

/* Draw a character at given point.
 * Point is specified in screen coordinates:
 * (0,0) is the top left hand corner, (1,0) is top right hand.
 * Assumes texture mode and matrices already set up.
 */
void draw_char(GLfloat x, GLfloat y, GLfloat scale,char c)
{
    GLfloat charx,chary,texx,texy,texdx,texdy;
    GLfloat cwidth,cheight;
    int i;

    cwidth=charwidth*scale;
    cheight=charheight*scale;


    i=(int)c-1;

    charx=((GLfloat)(i%12));
    chary=((GLfloat)(i/12));

    texdx=(charwidth)/256.0;
    texdy=(charheight)/256.0;

    texx=charx*charwidth/256.0;
    texy=chary*charheight/256.0;


    glBegin(GL_QUADS);
        glNormal3f(0.0,0.0,1.0);

        glTexCoord2f(texx,texy);
        glVertex3f(x,(texscreeny-y),0.0);

        glTexCoord2f(texx+texdx,texy);
        glVertex3f(x+cwidth,(texscreeny-y),0.0);

        glTexCoord2f(texx+texdx,texy+texdy);
        glVertex3f(x+cwidth,(texscreeny-y)-cheight,0.0);

        glTexCoord2f(texx,texy+texdy);
        glVertex3f(x,(texscreeny-y)-cheight,0.0);
    glEnd();
}

void draw_string(GLfloat x, GLfloat y,GLfloat scale, char *s)
{
    while (0!=*s) {
        draw_char(x,y,scale,*s);
        x+=charwidth*scale;
        s++;
    }
}

void draw_cursor(GLfloat scale)
{
    draw_char(termx*charwidth*scale,termy*charheight*scale,scale,'_');
    return;
}


/* Initialise terminal system */
int term_init(int sx, int sy)
{
    int i;

    texscreenx=sx;
    texscreeny=sy;
    for (i=0;i<TERMLINES;i++) {
        if (NULL==(termline[i]=malloc(TERMWIDTH+1))) {
            perror("malloc()");
            return -1;
        }
        termline[i][0]=0;
    }
    return 0;
}

/* Register new screen size */
int term_newscreensize(int sx,int sy) 
{
    texscreenx=sx;
    texscreeny=sy;
    return 0;
}

/* Initialise text-writing code */

int text_init()
{
    texture_frompng("charmap-256-10x20.png",&cmaptex,GL_LINEAR,GL_LINEAR);
    return 0;
}

/* Set appropriate GL parameters for writing text; assumes
 * 2d texturing enabled.
 */
int text_mode()
{
    glBindTexture(GL_TEXTURE_2D,cmaptex);
    return 0;
}

/* Draw contents of terminal to screen */
void term_draw()
{
    int i;
    const GLfloat scale=1.0;

    for (i=0;i<TERMLINES;i++) {
        draw_string(0.0,i*charheight*scale,scale,termline[i]);
    }
}

/* Scroll the terminal up by one line. */
void term_scroll()
{
    char *tmp;
    int i;

    tmp=termline[0];
    for (i=0;i<(termlines-1);i++) {
        termline[i]=termline[i+1];
    }
    tmp[0]=0; /* Clear the new line. */
    termline[termlines-1]=tmp;
}

/* Take a new line on the terminal, scrolling if necessary */

void term_nl()
{
    termx=0;
    termy++;
    if (termy>=termlines) {
        termy=termlines-1;
        term_scroll();
    }
}

/* Advance character by one position, taking new line if necessary */
void term_advance()
{
    termx++;
    if (termx>=termcols) {
        term_nl();
    }
}

/* Move terminal cursor to next tab stop */

void term_tab()
{
    termx+=termtabsize;
    termx-=(termx%termtabsize);
    if (termx>=termcols) {
        term_nl();
    }
}

/* Write a character at the given terminal character position */

void term_drawchar(int x, int y, char c)
{
    int i;
    if ( (y>=termlines) || (x>=termcols)) {
        return;
    }

    if (strlen(termline[y])<(x+1)) {
        /* Pad line out with spaces */

        for (i=0;0!=termline[y][i];i++);

        /* i is now the index of the NUL character */
        while (i<=(x+1)) {
            termline[y][i++]=' ';
        }
        termline[y][i]=0;
    }
    termline[y][x]=c;
}


/* Write a character to the terminal, advancing the cursor. */

void term_writechar(char c)
{
    switch(c) {
        case '\n':
            term_nl();
            break;
        case '\t':
            term_tab();
            break;
        case '\b':
            if (termx>0) {
                termx--;
            }
            term_drawchar(termx,termy,' ');
            break;
        default:
            term_drawchar(termx,termy,c);
            term_advance();
            break;
    }
}

/******************* Debugging aid and utility code *******************/

void yell(char *s) {
    static unsigned int c=0;

    printf("%u: %s\n",c++,s);
    fflush(stdout);
    return;
}

/* Return random real between 0.0 and 1.0 */
GLfloat rand01()
{
    return (GLfloat)((1.0*rand())/RAND_MAX);
}

GLfloat rand11()
{
    return (((GLfloat)rand()/RAND_MAX)-0.5)*2.0;
}

/* Return 0 if GL extension found, nonzero if not */

int findglext(char *extname)
{
    if (NULL==strstr((const char *)glGetString(GL_EXTENSIONS),extname)) {
        fprintf(stderr,"Couldn't find extension \"%s\"\n",extname);
        return -1;
    }
    fprintf(stderr,"Found extension \"%s\".\n",extname);
    return 0;
}

/*** Text and terminal code ****/

/* Create and return a pointer to new text state object;
 * return NULL on error.
 */

gltext *gltextNew(int cw,int ch,int screenx,int screeny,char *fname)
{
    gltext  *ts;

    if (NULL==(ts=malloc(sizeof(gltext)))) {
        perror("malloc()");
        return NULL;
    }

    if (NULL==(ts->cmapimg=texture_frompng(fname,&(ts->cmaptex),
        GL_LINEAR,GL_LINEAR))) {
        free(ts);
        return NULL;
    }

    ts->charwidth=cw;
    ts->charheight=ch;
    ts->screenx=screenx;
    ts->screeny=screeny;

    return ts;

}

void gltextResizeScreen(gltext *glt,int sx,int sy)
{
    glt->screenx=sx;
    glt->screeny=sy;
    return;
}

/* Destroy text state object, return nonzero on error, zero on success. */

int gltextDestroy(gltext *ts)
{
    free(ts->cmapimg);
    free(ts);
    return 0;
}

/* Set appropriate GL state to begin drawing text */
void gltextSetGL(gltext *ts)
{
    glBindTexture(GL_TEXTURE_2D,ts->cmaptex);
}

/* Draw a character at given point.
 * Point is specified in screen coordinates:
 * (0,0) is the top left hand corner, (1,0) is top right hand.
 * Assumes texture mode and matrices already set up.
 */
void gltextDrawChar(gltext *ts,GLfloat x, GLfloat y, GLfloat scale,char c)
{
    GLfloat charx,chary,texx,texy,texdx,texdy;
    GLfloat cwidth,cheight;
    int i;

    cwidth=ts->charwidth*scale;
    cheight=ts->charheight*scale;


    i=(int)c-1;

    charx=((GLfloat)(i%12));
    chary=((GLfloat)(i/12));

    texdx=(ts->charwidth)/256.0;
    texdy=(ts->charheight)/256.0;

    texx=charx*charwidth/256.0;
    texy=chary*charheight/256.0;


    glBegin(GL_QUADS);
        glNormal3f(0.0,0.0,1.0);

        glTexCoord2f(texx,texy);
        glVertex3f(x,(ts->screeny-y),0.0);

        glTexCoord2f(texx+texdx,texy);
        glVertex3f(x+cwidth,(ts->screeny-y),0.0);

        glTexCoord2f(texx+texdx,texy+texdy);
        glVertex3f(x+cwidth,(ts->screeny-y)-cheight,0.0);

        glTexCoord2f(texx,texy+texdy);
        glVertex3f(x,(ts->screeny-y)-cheight,0.0);
    glEnd();
}

void gltextDrawString(gltext *ts,
        GLfloat x, GLfloat y,GLfloat scale, char *s)
{
    while (0!=*s) {
        gltextDrawChar(ts,x,y,scale,*s);
        x+=charwidth*scale;
        s++;
    }
}


void gltermDestroyNonNull(glterm *gt)
{
    int i;

    for (i=0;i<gt->ncols;i++) {
        if (NULL!=gt->line[i]) {
            free(gt->line[i]);
        }
    }
}

glterm *gltermNew(int nlines,int ncols, gltext *tx)
{
    glterm  *gt;
    int i;

    if (NULL==(gt=malloc(sizeof(glterm)))) {
        perror("malloc()");
        return NULL;
    }

    if (NULL==(gt->line=malloc(sizeof(char *)*nlines))) {
        perror("malloc()");
        free(gt);
    }

    if (NULL==(gt->cbuf=cbufNew(TERMCHARBUFSIZE,CBUF_NONGROWABLE))) {
        free(gt->line);
        free(gt);
        return NULL;
    }

    gt->nlines=nlines;
    gt->ncols=ncols;
    gt->ts=tx;
    gt->tabsize=8;
    gt->echomode=0;
    gt->state=TERMSTATE_GROUND;
    gt->cursx=gt->cursy=0;
    gt->scrolltop=0;
    gt->scrollbot=nlines-1;

    for (i=0;i<nlines;i++) {
        gt->line[i]=NULL;
    }

    for (i=0;i<nlines;i++) {
        if (NULL==(gt->line[i]=malloc(ncols))) {
            perror("malloc()");
            gltermDestroyNonNull(gt);
            free(gt);
            return NULL;
        } else {
            gt->line[i][0]=0;
        }
    }

    return gt;
}

void gltermDrawChar(glterm *gt, int x, int y, char c)
{
    int i;
    int linelen;

    if ( (y>=gt->nlines) || (x>=gt->ncols) || (y<0) || (x<0) ) {
        return;
    }

    if ((linelen=strlen(gt->line[y]))<(x+1)) {
        /* Pad line out with spaces */

        i=linelen;
        if (0!=(gt->line[y][i])) {
            printf("argh\n");
            fflush(stdout);
            exit(-1);
        }

        /* i is now the index of the NUL character */
        while (i<(x+1)) {
            gt->line[y][i++]=' ';
        }
        gt->line[y][i]=0;
    }
    gt->line[y][x]=c;
}

void gltermDrawCursor(glterm *gt)
{
    glColor4f(1.0,1.0,0.0,1.0);
    gltextDrawChar(gt->ts,
        gt->cursx*gt->ts->charwidth,
        gt->cursy*gt->ts->charheight,
        1.0,
        '_');
}

void gltermScroll(glterm *gt,int nscroll,int topline,int botline)
{
    char *tmp=NULL;
    int i,n;
    int slen; /* No of lines in scroll region */

    slen=botline-topline+1;

    /* If everything is scrolled off, then just blank the
     * scrolled region.
     */

    if (nscroll>=slen) {
        for (i=topline;i<=botline;i++) {
            gt->line[i]=0;
        }
        return;
    }

    if (nscroll>0) {
        /* Scroll text up towards top of screen */

        for (n=0;n<nscroll;n++) {
            tmp=gt->line[topline];
            for (i=topline;i<botline;i++) {
                gt->line[i]=gt->line[i+1];
            }
            tmp[0]=0; /* Clear new line */
            gt->line[botline]=tmp;
        }
        return;
    } else {
        /* Scroll text down towards bottom of screen */

        nscroll=-nscroll;
        for (n=0;n<nscroll;n++) {
            tmp=gt->line[botline];
            for (i=botline;i>topline;i--) {
                gt->line[i]=gt->line[i-1];
            }
            tmp[0]=0;
            gt->line[topline]=tmp;
        }
        return;
    }

}


void gltermScroll1(glterm *gt)
{
    gltermScroll(gt,1,gt->scrolltop,gt->scrollbot);
}

void gltermLineStarve(glterm *gt)
{
    gt->cursy--;
    if (gt->cursy<0) { 
        gt->cursy=0;
        gltermScroll(gt,-1,gt->scrolltop,gt->scrollbot);
    }
}

void gltermRedraw(glterm *gt)
{
    int i;
    for (i=0;i<gt->nlines;i++) {
        gltextDrawString(gt->ts,
            0,i*(gt->ts->charheight),1.0,
            gt->line[i]);
    }
}

int gltermDestroy(glterm *gt)
{
    gltermDestroyNonNull(gt);
    free(gt);
    return 0;
}

void gltermClear(glterm *gt)
{
    int i;
    for (i=0;i<gt->nlines;i++) {
        gt->line[i][0]=0;
    }
}

/* Clear from given cursor position (in 1-based coords) to end of line */
void gltermClearToEOL(glterm *gt, int row, int col)
{
    if ( (row<0) || (col<0) || (row>gt->nlines) ||(col>gt->ncols) )
        { return ; }
#ifdef TERMDEBUG
    fprintf(stderr,"%ccleartoEOL at %d %d\n",7,row,col);fflush(stderr);
#endif

    gt->line[row][col]=0;

}

/* Clear from given cursor position (in 1-based coords) to end of screen */
void gltermClearToEOS(glterm *gt, int row, int col)
{
    int i;

    if ( (row<1) || (col<1) || (row>gt->nlines) ||(col>gt->ncols) )
        { return ; }

    gt->line[row][col]=0;

    for (i=row+1;i<gt->nlines;i++) {
        gt->line[i][0]=0;   
    }

}

/* Take a new line on the terminal, scrolling if necessary */

void gltermNL(glterm *tm)
{
    tm->cursy++;
    tm->cursx=0;
#ifdef TERMDEBUG
    fprintf(stderr,"termNL\n");fflush(stderr);
#endif
    if (tm->cursy>=tm->nlines) {
        tm->cursy=tm->nlines-1;
        gltermScroll1(tm);
    }
}
void gltermLF(glterm *tm)
{
#ifdef TERMDEBUG
    fprintf(stderr,"termLF\n");fflush(stderr);
#endif
    tm->cursy++;
    if (tm->cursy>=tm->nlines) {
        tm->cursy=tm->nlines-1;
        gltermScroll1(tm);
    }
}
void gltermCR(glterm *tm)
{

#ifdef TERMDEBUG
    fprintf(stderr,"termCR\n");fflush(stderr);
#endif
    tm->cursx=0;
}

/* Advance character by one position, taking new line if necessary */

void gltermAdvance(glterm *tm)
{
    tm->cursx++;
    if (tm->cursx>=tm->ncols) {
        gltermNL(tm);
    }
}

/* Move terminal cursor to next tab stop, limited by right margin. */

void gltermTab(glterm *tm)
{
    tm->cursx+=tm->tabsize;
    tm->cursx-=((tm->cursx)%(tm->tabsize));
    if (tm->cursx>=tm->ncols) {
        tm->cursx=tm->ncols-1;
    }
}

/* Process a character while in ground state */

static void gltermGroundWrite(glterm *tm, char c) {
    switch(c) {
        case ASCII_NUL:
        case ASCII_ENQ:
        case ASCII_BEL:
        case ASCII_VT:
        case ASCII_FF:
        case ASCII_SO:
        case ASCII_SI:
            /*
            printf("Got character %d\n",c);
            fflush(stdout);
            exit(-1);
            */
            break;
        case ASCII_ESC:
            /* State transition */
            tm->state = TERMSTATE_INESC;
            break;
        case ASCII_LF:
            gltermLF(tm);
            break;
        case ASCII_CR:
            gltermCR(tm);
            break;
        case ASCII_HT:
            gltermTab(tm);
            break;
        case '\b':
            if (tm->cursx>0) {
                tm->cursx--;
            }
            gltermDrawChar(tm,tm->cursx,tm->cursy,' ');
            break;
        default:
            gltermDrawChar(tm,tm->cursx,tm->cursy,c);
            gltermAdvance(tm);
            break;
    }
}

void gltermCursUp(glterm *tm)
{
    tm->cursy--;
    if (tm->cursy<0) { 
        tm->cursy=0;
    }
}
void gltermCursDown(glterm *tm)
{
    tm->cursy++;
    if (tm->cursy>=tm->nlines) { 
        tm->cursy=tm->nlines-1;
    }
}
void gltermCursLeft(glterm *tm)
{
    tm->cursx--;
    if (tm->cursx<0) { 
        tm->cursx=0;
    }
}
void gltermCursRight(glterm *tm)
{
    tm->cursx++;
    if (tm->cursx>=tm->ncols) { 
        tm->cursy=tm->ncols-1;
    }
}
void gltermCursHome(glterm *tm)
{
    tm->cursx=tm->cursy=0;
}
void gltermClearHome(glterm *tm)
{
    gltermClear(tm);
    gltermCursHome(tm);
}

/* Process a character while in INESC state */

/* See http://www.berkhan.com/basic/manual_7/manual_e/data/chap7-1.htm
 * for basic VT52 escapes.
 */

static void gltermInescWrite(glterm *tm,char c)
{
#ifdef TERMDEBUG
    fprintf(stderr,"> ESC %c\n",c);
    fflush(stderr);
#endif
    switch(c) {
        case 'A':   /* Cursor up */
            gltermCursUp(tm);
            break;
        case 'B':
            gltermCursDown(tm);
            break;
        case 'C':
            gltermCursRight(tm);
            break;
        case 'D':
            gltermCursLeft(tm);
            break;
        case 'E':
            gltermClearHome(tm);
            break;
        case 'F':
        case 'G':
            break;
        case 'H':
            gltermClearHome(tm);
            break;
        case 'I':
            gltermLineStarve(tm);
            break;
        case 'J':
            gltermClearToEOS(tm,tm->cursy,tm->cursx);
            break;
        case 'K':
#ifdef TERMDEBUG
            fprintf(stderr,"E-K\n");fflush(stderr);
#endif
            gltermClearToEOL(tm,tm->cursy,tm->cursx);
            break;
        case 'Y':
            tm->state=TERMSTATE_ESCY_WANTL;
            return;
        default:
            /*
            Handy for smoking out unhandled escape codes
            that some application is generating.
            */
#ifdef DIE_ON_UNKNOWN
            printf("NB unknown escape \'%c\'\n",c);
            fflush(stdout);
            exit(-1);
#endif
            break;



    }
    tm->state=TERMSTATE_GROUND;
}

/* The VT52 ESC Y sequence is of the form ESC Y l c
 * l and c are single characters giving the line and columns at which
 * the cursor must be positioned.
 * The actual values are found by subtracting 32 from l and c.
 * The line and column are zero-based.
 */
void gltermEscYWantlWrite(glterm *tm,char c)
{
    tm->escy_l = c-32;
    tm->state = TERMSTATE_ESCY_WANTC;
    return;
}
void gltermEscYWantcWrite(glterm *tm,char c)
{
    int escy_c;

    escy_c = c - 32;
    tm->cursx =     escy_c ;
    tm->cursy = tm->escy_l ;
    tm->state = TERMSTATE_GROUND;
#ifdef TERMDEBUG
    fprintf(stderr,"moveto(%d,%d)\n",tm->cursx,tm->cursy);fflush(stderr);
#endif
    return;
}

/* Send a char to a terminal, to be rendered onscreen */

void gltermWrite(glterm *tm,char c)
{
    switch(tm->state) {
        case TERMSTATE_GROUND:
            gltermGroundWrite(tm,c);
            break;
        case TERMSTATE_INESC:
            gltermInescWrite(tm,c);
            break;
        case TERMSTATE_ESCY_WANTL:
            gltermEscYWantlWrite(tm,c);
            break;
        case TERMSTATE_ESCY_WANTC:
            gltermEscYWantcWrite(tm,c);
            break;
    }
}

/* Called to pass a keypress event to the terminal.
 * The key character is stored in a FIFO buffer of fixed length.
 * If the buffer is full, -1 is returned.
 * If the character is successfully stored, then 0 is returned.
 * Nonzero indicates error.
 */

int gltermGotChar(glterm *tm, char c)
{
    if (tm->echomode) {
        gltermWrite(tm,c);
    }
    return cbufWrite(tm->cbuf,c);
}

/* Return -1 if no keys have been pressed, otherwise
 * return a character from the key buffer.
 */
int gltermGetChar(glterm *tm)
{
    return cbufRead(tm->cbuf);
}
