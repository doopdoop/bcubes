/*
 * Routines to handle circular buffer structures
 * Currently holds chars, although redefining CBUFTYPE and making a few
 * other modifications should generalize ot other types.
 */

#include <stdio.h>
#include <stdlib.h>

#include "cbuf.h"

cbuf_t *cbufNew(int size, cbufgrowth_t growth)
{
    cbuf_t *cbuf;

    if (NULL==(cbuf=malloc(sizeof(cbuf_t)))) {
        perror("malloc()");
        return NULL;
    }

    if (NULL==(cbuf->buf=malloc(sizeof(CBUFTYPE)*size))) {
        perror("malloc()");
        free(cbuf);
        return NULL;
    }

    cbuf->maxsize=size;
    cbuf->start=cbuf->size=0;
    cbuf->growth=growth;
    return cbuf;
    
}

int cbufDestroy(cbuf_t *cb)
{
#ifdef CBUFDEBUG
    fprintf(stderr,"cbufDestroy(%p)\n",(void *)cb);fflush(stderr);
#endif
    if (NULL==cb) { return -1; }
    if (NULL!=cb->buf) {
        free(cb->buf);
    }
    free(cb);
    return 0;
}

/* return no of items in cbuf */

unsigned int cbufGetSize(cbuf_t *cb)
{
    return cb->size;
}

/* Empty the buffer, wiping contents */

void cbufWipe(cbuf_t *cb)
{

#ifdef CBUFDEBUG
    fprintf(stderr,"cbufWipe()\n");fflush(stderr);
#endif

    cb->size=0;
    cb->start=0;
}

/* Attempt to resize buffer; return 0 on success */

int cbufResize(cbuf_t *cb, unsigned int newsize)
{


    CBUFTYPE *newbuf,*p;
    unsigned int n,i;

#ifdef CBUFDEBUG
    fprintf(stderr,"cbufResize(%u)\n",newsize);fflush(stderr);
#endif

    /* Return immediately if new size will not hold current data */

    if (newsize<cb->size) {
        return -1;
    }

    /* Try to allocate new buffer */

    if (NULL==(newbuf=malloc(sizeof(CBUFTYPE)*newsize))) {
        perror("malloc()");
        return -1;
    }

    /* Copy data into new buffer */

    p=newbuf;
    n=0;
    for (i=cb->start;n<cb->size;i=(i+1)%(cb->maxsize)) {
        *p++ = cb->buf[i];
        n++;
    }
    free(cb->buf);

    cb->start=0;
    cb->buf=newbuf;
    cb->maxsize=newsize;

    return 0;

}

/* Attempt to grow buffer.
 * Return 0 on success, indicating buffer size has increased.
 *
 * First, attempt to double buffer size. If that doesn't work,
 * keep halving the size difference until it works. If no
 * size increase is possible, return -1.
 */

int cbufGrow(cbuf_t *cb)
{
    unsigned int delta;

#ifdef CBUFDEBUG
    fprintf(stderr,"cbufGrow()\n");fflush(stderr);
#endif

    delta=cb->size;

    while (delta>0) {
        if (0==cbufResize(cb,cb->size+delta)) {
            return 0;
        }
        delta<<=1;
    }
    return -1;
}

/* Write a character into the buffer: return 0 on success. */

int cbufWrite(cbuf_t *cb, CBUFTYPE c)
{
    unsigned int end;

    if (cb->size==cb->maxsize) {
        if (CBUF_GROWABLE != cb->growth) {
            return -1;
        } else {
            if (0!=cbufGrow(cb)) {
                return -1;
            }
        }
    }
    /* At this point, there is sufficient room in the buffer */

    end=(cb->start+cb->size)%(cb->maxsize);

    /* end points to first empty slot */

    cb->buf[end]=c;
    cb->size++;

    return 0;
}


/* Return -1 if no chars in buffer, else read a char from buffer.
 * NB assumes CBUFTYPE==char
 */

int cbufRead(cbuf_t *cb)
{
    char c;

    if (0==cb->size) {
        return -1;
    } else {
        c=cb->buf[cb->start];
        cb->start = (cb->start+1)%(cb->maxsize);
        cb->size--;
        return ((int)c);
    }
}

/* Dump buffer contents in verbose debuggy manner */
/* NB requires CBUFTYPE==char */

void cbufDump(cbuf_t *cb)
{
    int i,n;

    printf("Buffer size %u maxsize %u\n",cb->size,cb->maxsize);
    printf("Buffer content string: \"");
    n=0;
    for (i=cb->start;n<cb->size;i=(i+1)%(cb->maxsize)) {
        putchar(cb->buf[i]);
        n++;
    }
    printf("\"\n");

    printf("Buffer contents:\n");

    for (i=0;i<cb->maxsize;i++) {
        printf("[%02d] %x <%c>",i,(unsigned int)(cb->buf[i]),
            (cb->buf[i] > 32) ? cb->buf[i] : '.');
        if (i==cb->start) {
            printf(" <-- start\n");
        } else {
            printf("\n");
        }
    }

}


char cbufRandomChar(void)
{
    const char printchars[]=
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "01234567890";
    float pcsize;
    int i;

    pcsize=(float)sizeof(printchars);

    i=(int) ((pcsize*rand())/(RAND_MAX+1.0));
    return printchars[i];
}
/* Self-test function */

int cbufTest(void)
{
    cbuf_t *cb;
    int i;


    /* Allocate a nongrowable buffer */

    if (NULL==(cb=cbufNew(8,CBUF_NONGROWABLE))) {
        printf("cbufNew() failed\n");
        return -1;
    }

    /* Buffer should not hold more than 8 chars. */

    for (i=0;i<8;i++) {
        if (0!=cbufWrite(cb,cbufRandomChar())) { return -1; }
    }
    /* Buffer should be full now, so next write should fail. */
    if (0==cbufWrite(cb,cbufRandomChar())) { return -1; }

    /* Good. Clear the buffer. */

    cbufWipe(cb);
    if (0!=cbufGetSize(cb)) { return -1; }

    /* move pointer to middle of buffer */

    for (i=0;i<4;i++) {
        if (0!=cbufWrite(cb,cbufRandomChar())) { return -1; }
    }
    for (i=0;i<4;i++) {
        if (-1==cbufRead(cb)) { return -1; }
    }
    if (0!=cbufGetSize(cb)) { return -1; }

    /* Write a string to wrap around the end */

    if (0!=cbufWrite(cb,'F')) { return -1; }
    if (0!=cbufWrite(cb,'o')) { return -1; }
    if (0!=cbufWrite(cb,'o')) { return -1; }
    if (0!=cbufWrite(cb,'B')) { return -1; }
    if (0!=cbufWrite(cb,'a')) { return -1; }
    if (0!=cbufWrite(cb,'r')) { return -1; }
    cbufDump(cb);

    /* Now read it back */
    if ('F'!=cbufRead(cb)) { return -1; }
    if ('o'!=cbufRead(cb)) { return -1; }
    if ('o'!=cbufRead(cb)) { return -1; }
    if ('B'!=cbufRead(cb)) { return -1; }
    if ('a'!=cbufRead(cb)) { return -1; }
    if ('r'!=cbufRead(cb)) { return -1; }
    if (-1!=cbufRead(cb)) { return -1; }

    cbufDestroy(cb);
    printf("Nongrowable tests OK\n");fflush(stdout);

    /* Make a new, growable buffer */

    if (NULL==(cb=cbufNew(8,CBUF_GROWABLE))) {
        printf("cbufNew() failed\n");
        return -1;
    }

    /* Buffer should hold more than 8 chars. */

    for (i=0;i<8;i++) {
        if (0!=cbufWrite(cb,cbufRandomChar())) { return -1; }
    }
    /* Buffer should be larger now, so next write should succeed. */
    if (0!=cbufWrite(cb,cbufRandomChar())) { return -1; }
    if (0!=cbufWrite(cb,cbufRandomChar())) { return -1; }
    if (0!=cbufWrite(cb,cbufRandomChar())) { return -1; }
    if (0!=cbufWrite(cb,cbufRandomChar())) { return -1; }
    if (0!=cbufWrite(cb,cbufRandomChar())) { return -1; }

    cbufDestroy(cb);

    printf("cbuf Self test passed\n");
    return 0;
        
}

#ifdef CBUFMAIN 
int main(int argc, char *argv[])
{
    return cbufTest();
}
#endif /* CBUFMAIN */
