#ifndef INCLUDED_GLUTIL_H
#define INCLUDED_GLUTIL_H
#include <GL/gl.h>
#include "cbuf.h"

#define ASCII_NUL   0
#define ASCII_ENQ   5
#define ASCII_BEL   7
#define ASCII_BS    8
#define ASCII_HT    9
#define ASCII_LF    10
#define ASCII_VT    11
#define ASCII_FF    12
#define ASCII_CR    13
#define ASCII_SO    14
#define ASCII_SI    15
#define ASCII_ESC   27

/* Structure holding texture image */
struct image {
    unsigned int width;
    unsigned int height;
    unsigned int bpp;
    unsigned int channels; /* 1 for grayscale, 3 for RGB, 4 for RGBA */
    unsigned char *data; /* RGBA format */
};

typedef struct image image_t;

struct gltext_s {
    unsigned int    charwidth,charheight;
    unsigned int    screenx,screeny;
    image_t *cmapimg;
    GLuint  cmaptex;
};

typedef struct gltext_s gltext;

#define TERMCHARBUFSIZE 1024    /* Size of key buffer */
enum termstate {
    TERMSTATE_GROUND =0,
    TERMSTATE_INESC = 1,
    TERMSTATE_ESCY_WANTL=2,
    TERMSTATE_ESCY_WANTC=3
};

typedef enum termstate termstate_t;


struct glterm_s {
    int nlines,ncols;   /* Dimensions of terminal area */
    int cursx,cursy;    /* Cursor position; zero-based. */
    gltext  *ts;    /* GL text drawing context */
    char    **line; /* Array of pointers to zero-terminated lines */

    int tabsize;    /* Tab width */
    cbuf_t *cbuf;   /* Buffer for typed characters */
    int echomode:1; /* 0=echo 1=noecho */
    termstate_t state;
    int escy_l; /* line no for VT52 ESC Y */
    int scrolltop,scrollbot; /* Scrolling region */
};

typedef struct glterm_s glterm;

#define TERMLINES 25
#define TERMWIDTH 80
extern const int termcols;
extern const int termlines;
extern const int termtabsize;
extern const int charwidth;
extern const int charheight;



struct image *image_frompng(char *); 
int image_destroy(image_t *);
GLenum imagegltype(image_t *);
int texture_fromimage(image_t *, GLuint *,GLint ,GLint);
image_t *texture_frompng(char *, GLuint *,GLint,GLint);
void draw_char(GLfloat , GLfloat , GLfloat,char );
void draw_string(GLfloat , GLfloat ,GLfloat, char *);
void draw_cursor(GLfloat);
int term_init(int,int);
int term_newscreensize(int,int); 
int text_init(void);
int text_mode(void);
void term_draw(void);
void term_scroll(void);
void term_nl(void);
void term_advance(void);
void term_tab(void);
void term_drawchar(int , int , char );
void term_writechar(char );
void yell(char *); 
GLfloat rand01(void);
GLfloat rand11(void);
int findglext(char *);

gltext *gltextNew(int cw, int ch, int screenx, int screeny, char *fname);
void gltextResizeScreen(gltext *glt, int sx, int sy);
int gltextDestroy(gltext *ts);
void gltextSetGL(gltext *ts);
void gltextDrawChar(gltext *ts,GLfloat x, GLfloat y, GLfloat scale,char c);
void gltextDrawString(gltext *, GLfloat , GLfloat ,GLfloat , char *);
void gltermDestroyNonNull(glterm *gt);
glterm *gltermNew(int nlines, int ncols, gltext *tx);
void gltermDrawChar(glterm *gt, int x, int y, char c);
void gltermDrawCursor(glterm *gt);
void gltermScroll(glterm *gt,int nscroll,int topline,int botline);
void gltermScroll1(glterm *gt);
void gltermLineStarve(glterm *gt);
void gltermUnscroll(glterm *gt);
void gltermRedraw(glterm *gt);
int gltermDestroy(glterm *gt);
void gltermClear(glterm *gt);
void gltermClearToEOL(glterm *gt, int row, int col);
void gltermClearToEOS(glterm *gt, int row, int col);
void gltermNL(glterm *tm);
void gltermLF(glterm *tm);
void gltermCR(glterm *tm);
void gltermAdvance(glterm *tm);
void gltermTab(glterm *tm);
void gltermCursUp(glterm *tm);
void gltermCursDown(glterm *tm);
void gltermCursLeft(glterm *tm);
void gltermCursRight(glterm *tm);
void gltermCursHome(glterm *tm);
void gltermClearHome(glterm *tm);
void gltermEscYWantlWrite(glterm *tm, char c);
void gltermEscYWantcWrite(glterm *tm, char c);
void gltermWrite(glterm *tm, char c);
int gltermGotChar(glterm *tm, char c);
int gltermGetChar(glterm *tm);
#endif
