#include <GL/gl.h>

#ifndef _QUAT_H

struct quat {
	GLfloat q0,q1,q2,q3;
};

typedef struct quat quat_t;


quat_t unitquat(void);
quat_t quatmultiply(quat_t, quat_t);
quat_t quatconjugate(quat_t);
quat_t quatadd(quat_t , quat_t);
quat_t quatsub(quat_t , quat_t);
GLfloat quatnorm2(quat_t );
quat_t quatscale(quat_t , GLfloat );
quat_t quatscalar(GLfloat );
quat_t quatvector3r(GLfloat , GLfloat y, GLfloat z);
quat_t quatinvert(quat_t);
quat_t quatnormalize(quat_t);
quat_t quatrotation(GLfloat, GLfloat, GLfloat, GLfloat);
GLfloat quattheta(quat_t);
void quatrotate(quat_t , GLfloat, GLfloat, GLfloat, GLfloat *, GLfloat *, GLfloat *);
void quatmatrix(quat_t ,GLfloat * );
void quatmatrixandinv(quat_t ,GLfloat *,GLfloat * );


#endif /* _QUAT_H */
