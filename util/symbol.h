#ifndef _SYMBOL_H
#define _SYMBOL_H

#undef SYMBOL_DEBUG

enum datatype {
	TYPE_INTEGER = 0,
	TYPE_DOUBLE = 1,
	TYPE_STRING = 2
};

typedef enum datatype datatype_t;

union symdata {
	int dat_int;
	double dat_dou;
	char *dat_str;
} ;

typedef union symdata symdata_t;

struct symbol {
	char *sym_name;		/* malloc()ed name of symbol */
	datatype_t sym_type;	/* Type of data pointed to by sym_ptr */
	symdata_t sym_data;	/* embedded number, or string ptr */
};

typedef struct symbol symbol_t;

struct symtab {
	unsigned int tab_size; /* Maximum number of symbols in table */
	unsigned int tab_no;   /* Current no of symbols in table */
	symbol_t *tab_data;    /* Array of symbols */
};

typedef struct symtab symtab_t;

symbol_t *symbol_new(char *name, datatype_t type, symdata_t data);
int symbol_destroy(symbol_t *sym);
int symbol_destroy_all(symbol_t *sym);
int symbol_isstring(symbol_t *sym) ;
int symbol_isnumeric(symbol_t *sym) ;
int symbol_isinteger(symbol_t *sym) ;
int symbol_isdouble(symbol_t *sym) ;
int symbol_setstring(symtab_t *symtab,char *name,char **d);
int symbol_setinteger(symtab_t *symtab,char *name,int *d);
int symbol_setdouble(symtab_t *symtab,char *name,double *d);
int symbol_setbool(symtab_t *symtab,char *name,int *i);


symtab_t *symtab_new(unsigned int size);
int symtab_destroy(symtab_t *symtab);
int symtab_destroy_all(symtab_t *symtab);
symbol_t *symtab_lookup(symtab_t *symtab, char *name);
int symtab_add(symtab_t *symtab, char *name, datatype_t type, symdata_t data);
int symtab_dump(symtab_t *symtab);
int symtab_dump_to_fd(symtab_t *, FILE *);
int symtab_purge(symtab_t *symtab);
int symtab_reset(symtab_t *symtab);
int symtab_dump_parseable(symtab_t *);
int symtab_dump_parseable_to_fd(symtab_t *,FILE *);
int symtab_print_string(char *);

#define IS_SYMBOL_CHAR(x) ( isalpha((x)) || isdigit((x)) || ((x)=='_') )

#endif /* _SYMBOL_H */
