#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "buffer.h"

/************************************************************************
 *
 * Functions dealing with the buffer datatype, containing growable
 * null-terminated strings
 *
 ***********************************************************************/

/* Create a new buffer object; return pointer to it or NULL on failure */
/* Ensure that the internal buffer is set to null-terminated "". */

buffer_t *buffer_new(int size) {
	buffer_t *buf;

	if (NULL==(buf=malloc(sizeof(buffer_t)))) {
		perror("buffer_new() unable to malloc()");
		return NULL;
	}

	if (NULL==(buf->str=malloc(size))) {
		perror("buffer_new() unable to malloc()");
		free(buf);
		return NULL;
	}
	buf->maxsize=size;
	buf->str[0]=0;
	return buf;
}

/* Deallocate a buffer, INCLUDING its string data. Return nonzero on error. */

int buffer_destroy(buffer_t *buf)
{
	free(buf->str);
	free(buf);
	return 0;
}

/* Take a string, and append it to the given buffer. Return zero on
 * success; nonzero indicates failure.
 * In case of failure, the contents of the buffer are guaranteed
 * to be unchanged.
 */

int buffer_append_string(buffer_t *buf, char *s)
{
	char *newbuf=NULL;
	int deltasize;
	int stringlen;

	stringlen=strlen(s);

	/* Is there enough space in the buffer already? */

	if (buf->maxsize-(1+strlen(buf->str)) < stringlen ) {
		/* No ; we need to reallocate */

		if (stringlen>buf->maxsize) {
			deltasize=2*stringlen; /* NB: heuristic */
		} else {
			deltasize=buf->maxsize;
		}

		while ( (NULL==newbuf) && (deltasize>=stringlen) ) {

			newbuf=realloc(buf->str, buf->maxsize+deltasize);
			/* If it didn't work, try a smaller one */
			if (NULL==newbuf) {
				deltasize--;
			}
		}
		/* Did we get a new array out in the end? */
		if (deltasize<=stringlen) {
			/* No - bail out. */
			perror("buffer_append_string: unable to realloc()");
			return -1;
		}
		/* Yes - adjust the structure to reflect this. */
		buf->str=newbuf;
		buf->maxsize+=deltasize;
	}

	/* At this point, the buffer is guaranteed to have enough space. */

	strncat(buf->str,s,buf->maxsize);
	return 0;
	
}


/* Increase the buffer size by a minimum of n characters,
 * buf preferably by a factor of two.
 * Return 0 on success.
 */

int buffer_inflate(buffer_t *buf, int n)
{

	int deltasize;
	char *newbuf=NULL;

	if (n>buf->maxsize) {
		deltasize=2*n; /* NB: heuristic */
	} else {
		deltasize=buf->maxsize;
	}

	while ( (NULL==newbuf) && (deltasize>=n) ) {

		newbuf=realloc(buf->str, buf->maxsize+deltasize);
		/* If it didn't work, try a smaller increment */
		if (NULL==newbuf) {
			deltasize--;
		}
	}
	/* Did we get a new array out in the end? */
	if ((NULL==newbuf) || (deltasize<=n) ) {
		/* No - bail out. */
		perror("buffer_append_string: unable to realloc()");
		return -1;
	}
	/* Yes - adjust the structure to reflect this. */
	buf->str=newbuf;
	buf->maxsize+=deltasize;
	return 0;
}

/* 
 * Clear the given buffer. Then read a line, terminated with \n or EOF,
 * from the given open file, into the buffer.
 * The '\n' is NOT stored in the buffer.
 * Return the number of characters read (INCLUDING '\n'),
 * or a negative number to indicate failure.
 * Return value of zero indicates EOF, as well as no characters read.
 */
int buffer_file_readline(buffer_t *buf, FILE *f)
{
	int pos=0; /* Index of next character to be written */
	int c;

	if (buf->maxsize<=2) {
		/* Get a decent-sized array */
		if (0!=buffer_inflate(buf,2)) {
			return -1;
		}
	}

	/* Buffer can hold at least one character plus terminating null */
	buf->str[0]=0;

	while (EOF!=(c=fgetc(f))) {

		if ('\n'==c) {
			/* Terminate the string and return. */
			buf->str[pos++]=0;
			return pos;
		}

		buf->str[pos++]=(char)c;

		/* Do we have enough space for another character? */
		if ((pos+1)>=buf->maxsize) {
			/* No - try to enlarge */
			if (0!=buffer_inflate(buf,2)) {
				/* No more space - null-terminate
				 * then exit.
				 */
				buf->str[pos]=0;
				return pos;
			}
		}
		/* At this point, guaranteed to have enough space
		 * for another char.
		 */
	}

	/* Program ends up here if EOF received */

	/* Null-terminate and return. */
	
	buf->str[pos]=0;
	return pos;

}


/* 
 * Clear the given buffer. Then read from a stream, terminated with EOF,
 * into the buffer.
 * Return the number of characters read.
 * or a negative number to indicate failure.
 * buffer guaranteed null-terminated.
 * Return value of zero indicates EOF, as well as no characters read.
 */
int buffer_file_readtoeof(buffer_t *buf, FILE *f)
{
	int pos=0; /* Index of next character to be written */
	int c;

	if (buf->maxsize<=2) {
		/* Get a decent-sized array */
		if (0!=buffer_inflate(buf,2)) {
			return -1;
		}
	}

	/* Buffer can hold at least one character plus terminating null */
	buf->str[0]=0;

	while (EOF!=(c=fgetc(f))) {


		buf->str[pos++]=(char)c;

		/* Do we have enough space for another character? */
		if ((pos+1)>=buf->maxsize) {
			/* No - try to enlarge */
			if (0!=buffer_inflate(buf,2)) {
				/* No more space - null-terminate
				 * then exit.
				 */
				buf->str[pos]=0;
				return pos;
			}
		}
		/* At this point, guaranteed to have enough space
		 * for another char.
		 */
	}

	/* Program ends up here if EOF received */

	/* Null-terminate and return. */
	
	buf->str[pos]=0;
	return pos;

}

/* Test the buffer code, returning nonzero on failure */

int buffer_test()
{
	buffer_t *buf;
	int i;
	int nlines=0;
	int nchars=0;
	int totalchars=0;
	FILE *f;

	char *randomword[] = {
		"\n",
		"In AD 2100, ","war was was beginning...\n",
		"What ","happen!\n",
		"Somebody ","set ","up ","us ","the ","bomb.\n",
		"We ","get ","signal!\n",
		"What!\n",
		"Main ","screen ","turn ","on.\n",
		"Its ","you!\n",
		"How ","are ","you ","gentlemen!!\n",
		"All ","your ","base ","are ","belong ","to ","us.\n",
		"What ","you ","say!!\n",
		"You ","are ","on ","the ","way ","to ","destruction.\n",
		"You ","have ","no ","chance ","to ","survive ",
		"make ","your ","time.\n",
		"HA ","HA ","HA ","HA!\n",
		"Take ","off ","every ","zig!\n",
		"Move ","zig ","for ","great ","justice.\n",
		NULL
	};

	if (NULL==(buf=buffer_new(2))) {
		return -1;
	}

	i=0;
	while (NULL!=randomword[i]) {
		if (0!=buffer_append_string(buf,randomword[i])) {
			buffer_destroy(buf);
			return -1;
		}
		printf("\nBuffer has length %d of max %d\n",
			(int)strlen(buf->str),buf->maxsize);
		printf("\n\nbuffer=<%s>\n",buf->str);
		i++;
	}

	if (0!=buffer_destroy(buf)) {
		return -1;
	}

	/* Test the line-by-line file reading */

	if (NULL==(buf=buffer_new(4))) {
		return -1;
	}

	if (NULL==(f=fopen("buffer.c","r"))) {
		perror("fopen(\"buffer.c\")");
		buffer_destroy(buf);
		return -1;
	}

	while (0!=(nchars=buffer_file_readline(buf,f))) {
		nlines++;
		totalchars+=nchars;
		printf("%d: %s\n",nlines,buf->str);
	}
	fclose(f);

	printf("%d lines, %d characters\n",nlines,totalchars);

	return buffer_destroy(buf);

}
