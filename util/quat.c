#include <stdio.h>
#include <stdlib.h>

#include <math.h>

#include "quat.h"


/* Return a unit quaternion */

quat_t unitquat(void) {
	quat_t q;
	q.q0=1.0; q.q1=0.0; q.q2=0.0; q.q3=0.0;
	return q;
}

/*
 * Return the quaternion product of two quaternions.
 *
 * ( a0, a ) ( b0, b) =  ( a0*b0 - a.b , a0*a + b0*b + a x b )
 *
 */

quat_t quatmultiply(quat_t a, quat_t b)
{
	quat_t q;

	q.q0 = a.q0 * b.q0 
		- a.q1*b.q1
		- a.q2*b.q2
		- a.q3*b.q3;
	
	q.q1 =    a.q0 * b.q1
		+ b.q0 * a.q1
		+ a.q2 * b.q3 - a.q3 * b.q2;

	q.q2 =    a.q0 * b.q2
		+ b.q0 * a.q2
		+ a.q3 * b.q1 - a.q1 * b.q3;

	q.q3 =    a.q0 * b.q3
		+ b.q0 * a.q3
		+ a.q1 * b.q2 - a.q2 * b.q1;
	return q;
}

/* Return the conjugate of a quaternion */

quat_t quatconjugate(quat_t q)
{
	quat_t qq;

	qq.q0 = q.q0;
	qq.q1 = - q.q1;
	qq.q2 = - q.q2;
	qq.q3 = - q.q3;
	return qq;
}

/* Return the sum of two quaternions */

quat_t quatadd(quat_t a, quat_t b)
{
	quat_t q;

	q.q0 = a.q0 + b.q0;
	q.q1 = a.q1 + b.q1;
	q.q2 = a.q2 + b.q2;
	q.q3 = a.q3 + b.q3;
	return q;
}

/* Return the difference of two quaternions */

quat_t quatsub(quat_t a, quat_t b)
{
	quat_t q;

	q.q0 = a.q0 - b.q0;
	q.q1 = a.q1 - b.q1;
	q.q2 = a.q2 - b.q2;
	q.q3 = a.q3 - b.q3;
	return q;
}

/* Return the squared norm of a quaternion */

GLfloat quatnorm2(quat_t q)
{
	return  q.q0*q.q0 + q.q1*q.q1 + q.q2*q.q2 + q.q3*q.q3;
}

/* Return the quaternion multiplied by the scalar */

quat_t quatscale(quat_t q, GLfloat s)
{
	quat_t qq;
	qq.q0 = q.q0 * s;
	qq.q1 = q.q1 * s;
	qq.q2 = q.q2 * s;
	qq.q3 = q.q3 * s;
	return qq;
}

/* Return a scalar quaternion */

quat_t quatscalar(GLfloat s)
{
	quat_t q;
	q.q0 = s;
	q.q1 = q.q2 = q.q3 = 0.0;
	return q;
}

/* Return a vector quaternion */

quat_t quatvector3r(GLfloat x, GLfloat y, GLfloat z)
{
	quat_t q;
	q.q0 = 0.0;
	q.q1 = x;
	q.q2 = y;
	q.q3 = z;
	return q;
}

/* Invert a quaternion */

quat_t quatinvert(quat_t q)
{
	return quatscale(quatconjugate(q),1.0/quatnorm2(q));
}

/* Normalize a quaternion */

quat_t quatnormalize(quat_t q)
{
	return quatscale(q,1.0/sqrt(quatnorm2(q)));
}

/* Construct the quaternion corresponding to a right-handed rotation of
 * theta radians about the given vector.
 */

quat_t quatrotation(GLfloat theta, GLfloat x, GLfloat y, GLfloat z)
{
	GLfloat st,ct;
	quat_t q;

	st = sin(0.5*theta);
	ct = cos(0.5*theta);

	q.q0 = ct;
	q.q1 = x*st;  
	q.q2 = y*st;  
	q.q3 = z*st;  
	return q;
}

/* Extract the angle from a rotation quaternion */

GLfloat quattheta(quat_t q)
{
	return 2.0*acos(q.q0);
}

/* Rotate the given vector by the given quaternion */

void quatrotate(quat_t q, GLfloat x, GLfloat y, GLfloat z, GLfloat *xx, GLfloat *yy, GLfloat *zz)
{
	quat_t p,qq;

	p = quatvector3r(x,y,z);

	qq = quatmultiply(q,quatmultiply(p,quatconjugate(q)));

	*xx = qq.q1;
	*yy = qq.q2;
	*zz = qq.q3;
	return;
}

/* Turn a quaternion into a matrix */

void quatmatrix(quat_t q,GLfloat *m )
{
	GLfloat w,x,y,z;

	w = q.q0; x = q.q1; y=q.q2; z=q.q3;

	m[ 0] = 1 - 2*y*y - 2*z*z;
	m[ 1] = 2*x*y + 2*w*z;
	m[ 2] = 2*x*z - 2*w*y;
	m[ 3] = 0;

	m[ 4] = 2*x*y - 2*w*z;
	m[ 5] = 1 - 2*x*x - 2*z*z;
	m[ 6] = 2*y*z + 2*w*x;
	m[ 7] = 0;

	m[ 8] = 2*x*z + 2*w*y;
	m[ 9] = 2*y*z - 2*w*x;
	m[10] = 1 - 2*x*x - 2*y*y;
	m[11] = 0;

	m[12] = 0;
	m[13] = 0;
	m[14] = 0;
	m[15] = 1;

	return;

}

/* Turn a quaternion into a matrix and its inverse (ie transpose) */

void quatmatrixandinv(quat_t q,GLfloat *m,GLfloat *mi )
{
	GLfloat w,x,y,z;

	w = q.q0; x = q.q1; y=q.q2; z=q.q3;

	mi[ 0] = m[ 0] = 1 - 2*y*y - 2*z*z;
	mi[ 4] = m[ 1] = 2*x*y + 2*w*z;
	mi[ 8] = m[ 2] = 2*x*z - 2*w*y;
	mi[12] = m[ 3] = 0;

	mi[ 1] = m[ 4] = 2*x*y - 2*w*z;
	mi[ 5] = m[ 5] = 1 - 2*x*x - 2*z*z;
	mi[ 9] = m[ 6] = 2*y*z + 2*w*x;
	mi[13] = m[ 7] = 0;

	mi[ 2] = m[ 8] = 2*x*z + 2*w*y;
	mi[ 6] = m[ 9] = 2*y*z - 2*w*x;
	mi[10] = m[10] = 1 - 2*x*x - 2*y*y;
	mi[14] = m[11] = 0;

	mi[ 3] = m[12] = 0;
	mi[ 7] = m[13] = 0;
	mi[11] = m[14] = 0;
	mi[15] = m[15] = 1;

	return;

}
