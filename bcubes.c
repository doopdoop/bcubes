#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <unistd.h>
#include <math.h>

#include <png.h>

#include "util/quat.h"
#include "util/cbuf.h"
#include "util/glutil.h"

/* pty things */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <termios.h>
#include <signal.h>

#ifdef __APPLE__
#include <util.h>
#else
#include <pty.h>
#endif

int masterfd,slavefd;
int masterfdopen=0; /* nonzero if masterfd is a valid fd */
int dieflag=0; /* Set to one for prog to terminate on next frame */
pid_t pid;
fd_set readfds,writefds;
struct timeval tv;


/* Handler for SIGCHLD */
void sigchld_handler(int foo)
{
    /* Wait for any child to die; return immediately if
     * there are none ready to die.
     */

    switch(waitpid(-1,NULL,WNOHANG)) {
        case 0:
            fprintf(stderr,"SIGCHLD with no child!\n");
            fflush(stderr);
            break;
        case 1:
            perror("waitpid()");
            fflush(stderr);
            break;
        default:
            dieflag=1;
            break;
    }
    return;
}

void install_sigchld()
{
    struct sigaction sa;

    /* Set signal handling function */
    sa.sa_handler = sigchld_handler;

    /* Don't block other signals */
    sigemptyset(&sa.sa_mask);

    /* Don't call me if the child has just stopped, rather than died */
    sa.sa_flags = SA_NOCLDSTOP;

    /* Install */

    if (0!=sigaction(SIGCHLD,&sa,NULL)) {
        perror("sigaction()");
        return;
    }

    return;

}


int pty_init(int nrows,int ncols,int xpix,int ypix)
{
    struct winsize ws;

    ws.ws_row=nrows;
    ws.ws_col=ncols;
    ws.ws_xpixel=xpix;
    ws.ws_ypixel=ypix;

    if (0!=openpty(&masterfd,&slavefd,NULL,NULL,&ws))  {
        perror("openpty()");
        return -1;
    }
    masterfdopen=1;

    install_sigchld();

    char *shellpath = getenv("SHELL");
    if (NULL==shellpath) { shellpath="/bin/sh"; }

    switch(pid=fork()) {
        case -1:
            perror("fork()");
            return -1;
        case 0:
            /* Child process */

            /* Don't pass confusing environment vars */
            unsetenv("LINES");
            unsetenv("COLUMNS");
            unsetenv("TERMCAP");

            /* Become session leader */

            setsid();

            /* Make stdin, stdout, stderr talk to pty */
            dup2(slavefd,0);
            dup2(slavefd,1);
            dup2(slavefd,2);

            /* Close the master side of the pty */

            close(masterfd);

            setenv("TERM","vt52",1);

            execlp(shellpath,shellpath,NULL);
            perror("execlp()");
            return -1;
        default:
            /* Parent */

            break;
    }

    return 0;
}


gltext  *glt=NULL;
glterm  *term=NULL;

struct cuboid {
    GLfloat width,height,length,x,y,dist,vel,theta,omega;
    GLfloat pcolor[3]; /* polygon color */
    GLfloat ocolor[3]; /* outline color */
};
typedef struct cuboid cuboid_t;

const GLfloat nearclip=0.1;
#define NCUBOIDS 40
cuboid_t cuboid[NCUBOIDS];
const GLfloat maxcubdist=100.0;
const GLfloat cuboidvel=0.5;
const GLfloat tunrad=0.0;
GLfloat vieweromega=0.0;
const GLfloat alphamax=0.5;
GLfloat viewertheta=0.0;

SDL_Window *screen; 
SDL_Renderer *renderer;
SDL_GLContext glcx;

SDL_Rect screenrect;        /* Screen geometry */
int screenx,screeny;

GLuint wirelist=0,solidlist=0;

int mousex=0,mousey=0;      /* Mouse status */
int leftbutdown=0;
int rightbutdown=0;
int midbutdown=0;
int bx=0,by=0;

Uint16 timetop,timebot;


GLfloat sphererad=0.0; /* Radius of trackball sphere */
/* SDL button values for mouse wheel motion */
#define WHEELUP 4
#define WHEELDOWN 5

int glinit=0; /* Set to 1 once GL has been set up */
unsigned int frameno=0;

/* Things which are global but shouldn't be */
volatile int frameflag=0;   /* Set to 1 after frame timer expires */
#define FRAMETIMEMS 20  /* 40ms <=> 25 fps */

#define NBLURS 15   /* no of motion-blur frames */
/* Quaternion for global orientation */
quat_t orquat;
quat_t borquat[NBLURS]; /* blurred orientations */
GLfloat bpos[NBLURS*3]; /* blurred positions */

GLuint basetex;     /* Base texture */
image_t *backimg=NULL;
GLfloat dist=5;
GLfloat geomx=0,geomy=0;

/* pty things */
    int masterfd,slavefd;
    pid_t pid;
    fd_set readfds,writefds;
    struct timeval tv;

void init_blurs()
{
    int i;

    for (i=0;i<NBLURS;i++) {
        borquat[i]=orquat;
        bpos[i]=geomx;
        bpos[i+1]=geomy;
        bpos[i+2]=dist;
    }
}

void propagate_blurs()
{
    int i;
    for (i=NBLURS-1;i>0;i--) {
        borquat[i]=borquat[i-1];
        bpos[3*i]=bpos[3*(i-1)];
        bpos[3*i+1]=bpos[3*(i-1)+1];
        bpos[3*i+2]=bpos[3*(i-1)+2];
    }
    borquat[0]=orquat;
    bpos[0]=geomx;
    bpos[1]=geomy;
    bpos[2]=dist;
}


int init_cuboids()
{
    int i,j;
    for (i=0;i<NCUBOIDS;i++) {
        cuboid[i].width=0.5*(1.0+rand01());
        cuboid[i].height=0.5*(1.0+rand01());
        cuboid[i].length=1.0+2.0*rand01();
        cuboid[i].vel=cuboidvel*0.5*(1.0+rand01());
        do {
            cuboid[i].x=5.0*rand11();
            cuboid[i].y=5.0*rand11();
        } while ((cuboid[i].x*cuboid[i].x)+(cuboid[i].y*cuboid[i].y)
            < tunrad*tunrad);
        cuboid[i].dist=maxcubdist*rand01();
        cuboid[i].theta=360.0*rand01();
        cuboid[i].omega=10.0*rand11();
        for (j=0;j<3;j++) {
            cuboid[i].pcolor[j]=0.5*(1.0+rand01());
            cuboid[i].ocolor[j]=0.7+0.3*rand01();
        }
    }
    return 0;
}

void draw_cuboids(GLfloat alpha,GLfloat disp)
{
    int i;

    glDisable(GL_TEXTURE_2D);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(1.0,1.0);
    glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
    glMaterialf(GL_FRONT,GL_SHININESS,125);

    for (i=0;i<NCUBOIDS;i++) {
        glLoadIdentity();
        glRotatef(viewertheta-disp*vieweromega,0.0,0.0,1.0);
        glTranslatef(cuboid[i].x,cuboid[i].y,
            -cuboid[i].dist-disp*cuboid[i].vel);
        glRotatef(cuboid[i].theta-disp*cuboid[i].omega,0.0,0.0,1.0);
        glScalef(cuboid[i].width,cuboid[i].height,cuboid[i].length);
        glColor4f(  cuboid[i].pcolor[0],
                cuboid[i].pcolor[1],
                cuboid[i].pcolor[2],
                alpha);
        glCallList(solidlist);
        glColor4f(  cuboid[i].ocolor[0],
                cuboid[i].ocolor[1],
                cuboid[i].ocolor[2],
                alpha);
        glCallList(wirelist);
    }
}

void newcuboid(int i)
{
    int j;

    cuboid[i].width=0.5*(1.0+rand01());
    cuboid[i].height=0.5*(1.0+rand01());
    cuboid[i].length=1.0+2.0*rand01();
    cuboid[i].vel=cuboidvel*0.5*(1.0+rand01());
    do {
        cuboid[i].x=5.0*rand11();
        cuboid[i].y=5.0*rand11();
    } while ((cuboid[i].x*cuboid[i].x)+(cuboid[i].y*cuboid[i].y)
            < tunrad*tunrad);
    cuboid[i].dist=maxcubdist;
    cuboid[i].theta=360.0*rand01();
    cuboid[i].omega=10.0*rand11();
    for (j=0;j<3;j++) {
        cuboid[i].pcolor[j]=0.5*(1.0+rand01());
        cuboid[i].ocolor[j]=0.7+0.3*rand01();
    }
}

void advect_cuboids()
{
    int i;

    for (i=0;i<NCUBOIDS;i++) {
        cuboid[i].dist -= cuboid[i].vel;
        cuboid[i].theta+=cuboid[i].omega;
        if (cuboid[i].dist < -cuboid[i].length) {
            newcuboid(i);
        }
    }
}


/* Called to resize the screen to given dimensions.
 * Returns 0 on success, nonzero on failure.
 */

int vidresize(int w, int h) {
    SDL_Window *newscreen=NULL;

    if (NULL==(newscreen=SDL_CreateWindow(
        "blurcubes",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        w,h,
        SDL_WINDOW_OPENGL))) {
        fprintf(stderr, "SDL_SetVideoMode() failed\n");
        return -1;
    } else {
        screen=newscreen;


        glcx = SDL_GL_CreateContext(screen);
        if (NULL==glcx) { fprintf(stderr,"SDL_GL_CreateContext failed\n"); return -1; }
        SDL_GL_MakeCurrent(screen,glcx);
        screenx = w;
        screeny = h;

        screenrect.x=screenrect.y=0;
        screenrect.w=w;
        screenrect.h=h;

        if (glinit) {
            glViewport(0,0,screenx,screeny);
            if (NULL!=glt) {
                gltextResizeScreen(glt,screenx,screeny);
            }
        }

        if (screenx>screeny) {
            sphererad=0.5*screeny;
        } else {
            sphererad=0.5*screenx;
        }
    }
    return 0;
}

const GLfloat vertices[8][3] = {
    { -0.5, -0.5, -0.5 },
    {  0.5, -0.5, -0.5 },
    {  0.5,  0.5, -0.5 },
    { -0.5,  0.5, -0.5 },
    { -0.5, -0.5,  0.5 },
    {  0.5, -0.5,  0.5 },
    {  0.5,  0.5,  0.5 },
    { -0.5,  0.5,  0.5 },
};

const int quads[6][4] = {
    { 0, 1, 2, 3},
    { 0, 4, 5, 1},
    { 1, 5, 6, 2},
    { 2, 6, 7, 3},
    { 3, 7, 4, 0},
    { 4, 7, 6, 5},
};

const GLfloat normals[6][3] = {
    {  0.0,  0.0, -1.0 },
    {  0.0, -1.0,  0.0 },
    {  1.0,  0.0,  0.0 },
    {  0.0,  1.0,  0.0 },
    { -1.0,  0.0,  0.0 },
    {  0.0,  0.0,  1.0 },
};


void draw_cube(GLenum mode)
{
    int i,j;
    glBegin(mode);
    for (i=0;i<6;i++) {
        glNormal3f(normals[i][0],normals[i][1],normals[i][2]);
        for (j=0;j<4;j++) {
            glVertex3f(
                vertices[quads[i][j]][0],
                vertices[quads[i][j]][1],
                vertices[quads[i][j]][2]
            );
        }
    }
    glEnd();

}



/* Called to initialise GL (as opposed to SDL's GL interface).
 * Must only be called once the SDL video subsystem has been set up.
 */

void init_gl()
{
    GLfloat ambientlight[] = { 0.5,0.5,0.5};
    GLfloat diffuselight[] = { 0.5,0.5,0.5};

    /* Set up us the viewport */

    glViewport(0,0,screenx,screeny);
    /* Goraud Shading */
    glShadeModel(GL_SMOOTH);

    /* Background colour */

    glClearColor(0.0,0.0,0.0,0.0);
    glClearDepth(1.0);

    /* Enable blending */

    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    glEnable(GL_TEXTURE_2D);

    /* Set up lighting */

    glLightfv(GL_LIGHT1,GL_AMBIENT,ambientlight);
    glLightfv(GL_LIGHT1,GL_DIFFUSE,diffuselight);
    glEnable(GL_LIGHT1);

    glEnable(GL_LIGHTING);

    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);


    /* Set up display lists */

    if (0==(solidlist=glGenLists(1))) {
        fprintf(stderr,"Out of display lists\n");
        exit(-1);
    }
    if (0==(wirelist=glGenLists(1))) {
        fprintf(stderr,"Out of display lists\n");
        exit(-1);
    }

    glNewList(solidlist,GL_COMPILE);
    draw_cube(GL_QUADS);
    glEndList();

    glNewList(wirelist,GL_COMPILE);
    draw_cube(GL_LINE_LOOP);
    glEndList();

    glEnable(GL_POLYGON_OFFSET_FILL);

    glEnable(GL_TEXTURE_2D);
    glLineWidth(3.0);

    init_blurs();
    glinit=1;
}

void draw_gl()
{

    int i;
    static GLfloat nrenders=0;

    /* Set for perspective projection */

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0,(float)screenx/(float)screeny,nearclip,1024.0 );
    glMatrixMode(GL_MODELVIEW);

    glDepthMask(GL_TRUE);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glRotatef(viewertheta,0.0,0.0,1.0);
    viewertheta+=vieweromega;
    vieweromega+=0.002*sin((nrenders++)*M_PI/360.0);

    glDisable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glDepthMask(GL_FALSE);

    for (i=NBLURS-1;i>=0;i--) {
        GLfloat alpha;

        alpha=0.2*alphamax*(1.0-((GLfloat)i/(NBLURS+1.0)));
        draw_cuboids(alpha,(GLfloat)i);
    }
    glDepthMask(GL_TRUE);
    draw_cuboids(alphamax,0.0);

    /* Now draw some letters */


    glDepthMask(GL_FALSE);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glEnable(GL_TEXTURE_2D);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0,(GLfloat)screenx,0.0,(GLfloat)screeny);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    text_mode();
    glDisable(GL_LIGHTING);
    glColor4f(0.0,0.5,0.7,1.0);
    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);

    glColor4f(0.0,1.0,0.0,0.7);
    glEnable(GL_TEXTURE_2D);
    gltextSetGL(term->ts);
    gltermRedraw(term);
    
    
    return ;
}


/* Process mouse motion from x0,y0 to x1,y1 */

void mouserotatemotion(int x0,int y0, int x1,int y1) {

    GLfloat mx,my,m,s;
    GLfloat theta;
    quat_t rotquat;

    s = sphererad;
    my = (GLfloat)(x1-x0);
    mx = (GLfloat)(y1-y0);
    m=sqrt(mx*mx+my*my);

    if ((m>0) && (m<s)) {
        theta = m/s;

        mx /= m;
        my /= m;

        rotquat = quatrotation(theta,mx,my,0.0);
        orquat = quatmultiply(rotquat,orquat);
    }

}

void mousezoommotion(int dz)
{
    const GLfloat zoomscale=0.02;
    dist+=zoomscale*dz;
    return;
}

void mousetransmotion(int dx,int dy)
{
    const GLfloat transscale=0.02;
    geomx+=transscale*dx;
    geomy-=transscale*dy; /* GL uses left-handed coordinates */
    return;
}


/* Clear up and quit. */
void quit_all() {

    SDL_ShowCursor(SDL_ENABLE);
    SDL_Quit();
    if (masterfdopen) {
        close(masterfd);
    }
    exit(0);
}

/* Called after a frame-time interval has elapsed.
 *
 * In order to avoid calculating lots of frames which will not
 * ever be displayed, the frame rate is locked at 1000/FRAMETIMEMS Hz --
 * unfortunately there does not seem to be a reliable and portable method
 * for waiting around until the next vertical blanking interval.
 *
 * At the top of the main loop, frameflag is set to 0, and a timer is set up
 * which will call frametime_callback() in FRAMETIMEMS milliseconds. At
 * the bottom of the main loop is another loop which checks if frameflag==1,
 * and if not, sleeps until the next SDL event. 
 * When frametime_callback() is run from the timer, it sets frameflag=1,
 * and pushes a dummy user event into the queue to wake up the event loop.
 *
 * Before, the code used to work out how many milliseconds it had to sleep
 * before FRAMTIMEMS ms had passed since the top of the main loop, and it
 * would SDL_Delay() for that period of time. Unfortunately, this meant it
 * would often miss keyboard or mouse events that would have to wait until
 * the next frame to be processed -- this lag can be noticeable.
 *
 * Doing it the current way means that any input events will be processed
 * while the system is idling before the next frame.
 */

SDL_Event frameevent;
Uint32 frametime_callback(Uint32 interval, void *param)
{
    /* Set the frame flag for main loop to proceed */
    frameflag=1;

    /* Push a dummy event to wake up main loop */

    frameevent.type = SDL_USEREVENT;
    SDL_PushEvent(&frameevent);

    return 0; /* Retval of zero removes the timer */
}

void write_to_term(const char c) {
    /* gltermWrite(term,c); this just doubles-up all the chars */
    if (0>write(masterfd,&c,1)) { perror("write"); }
}

void event_keydown(SDL_Scancode scancode) {
    switch(scancode) {
        case SDL_SCANCODE_RETURN:
        case SDL_SCANCODE_RETURN2:
        case SDL_SCANCODE_KP_ENTER:
            write_to_term('\n');
            break;
        case SDL_SCANCODE_TAB:
            write_to_term('\t');
            break;
        case SDL_SCANCODE_BACKSPACE:
            write_to_term('\b');
            break;
        default:
            break;
    }
    return;
}

void event_textinput(SDL_TextInputEvent tevent) {
    char *p=tevent.text;
    while (0!=*p) {
        write_to_term(*p++);
    }
    return;
}


int main(int argc, char *argv[])
{
    SDL_Event event;

    screenx=80*10;
    screeny=25*20;

    /* Fork off child shell etc */
    pty_init(25,80,25*20,80*10);

    /* Initialize SDL */

    if ( 0 > SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER)) {
        fprintf(stderr,"main: SDL_Init() failed\n");
        return -1;
    }

    /* Set GL options: >=15-bit colour, 16-bit Z, double-buffered. */
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#ifdef __APPLE__
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
#endif
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,5);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,5);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,5);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

    /* Make us a window at 32bpp */


    vidresize(screenx,screeny);

    /* Initialize GL */

    init_gl();
    if (NULL==(glt=gltextNew(
        10,20,screenx,screeny,"charmap-256-10x20.png"))) {
        return -1;
    }

    if (NULL==(term=gltermNew(25,80,glt))) {
        return -1;
    }

    init_cuboids();
    orquat = quatrotation(0.0,0.0,0.0,1.0);

    /* Main loop */

    while(1) {
        char keychar=0;

        timetop=SDL_GetTicks();

        /* Set up a timer to callback after frame time elapsed */

        frameflag=0;
        SDL_AddTimer(FRAMETIMEMS,frametime_callback,NULL);


        /* Render frame to screen */
        draw_gl();

        if (0==((int)(frameno/25.0)%2)) {
            gltermDrawCursor(term);
        }
        SDL_GL_SwapWindow(screen);

        propagate_blurs();
        advect_cuboids();

        /* Check stdin for data */

        do {

            if (dieflag) {
                quit_all();
            }

            SDL_StartTextInput();

            /* Poll for events */
            if (SDL_WaitEvent(NULL)) {
                while (SDL_PollEvent(&event)) {

                switch(event.type) {
                    case SDL_TEXTINPUT:
                        event_textinput(event.text);
                        break;
                    case SDL_KEYDOWN:
                        event_keydown(event.key.keysym.scancode);
                        break;
                    case SDL_QUIT:
                        SDL_ShowCursor(SDL_ENABLE);
                        return 0;
                        break;
                    case SDL_MOUSEBUTTONDOWN:
                        switch(event.button.button) {
                            case SDL_BUTTON_LEFT:
                            leftbutdown=1;
                            bx=event.button.x;
                            by=event.button.y;
                            SDL_ShowCursor(SDL_DISABLE);
                            break;
                            case SDL_BUTTON_RIGHT:
                            rightbutdown=1;
                            bx=event.button.x;
                            by=event.button.y;
                            SDL_ShowCursor(SDL_DISABLE);
                            break;
                            case SDL_BUTTON_MIDDLE:
                            midbutdown=1;
                            bx=event.button.x;
                            by=event.button.y;
                            SDL_ShowCursor(SDL_DISABLE);
                            break;
                            case WHEELDOWN:
                            dist+=1.0;
                            break;
                            case WHEELUP:
                            dist-=1.0;
                            break;
                            default:
                            break;
                        }
                        break;
                    case SDL_MOUSEBUTTONUP:
                        switch(event.button.button) {
                            case SDL_BUTTON_LEFT:
                            leftbutdown=0;
                            if (!leftbutdown &&
                                !midbutdown) {
                            SDL_ShowCursor(SDL_ENABLE);
                            }
                            break;
                            case SDL_BUTTON_RIGHT:
                            rightbutdown=0;
                            if (!leftbutdown &&
                                !midbutdown) {
                            SDL_ShowCursor(SDL_ENABLE);
                            }
                            break;
                            case SDL_BUTTON_MIDDLE:
                            midbutdown=0;
                            if (!leftbutdown &&
                                !rightbutdown) {
                            SDL_ShowCursor(SDL_ENABLE);
                            }
                            break;
                            default:
                            break;
                        }
                        break;
                    case SDL_MOUSEMOTION:
                        mousex = event.motion.x;
                        mousey = event.motion.y;
                        if ((0!=event.motion.xrel) ||
                            (0!=event.motion.yrel)) {
                            if (leftbutdown) {
                            mouserotatemotion(bx,by,
                                mousex,mousey);
                                SDL_WarpMouseGlobal(bx,by);
                                mousex=bx; mousey=by;
                            } else if (rightbutdown) {
                            mousezoommotion(
                                event.motion.y-by);
                                SDL_WarpMouseGlobal(bx,by);
                                mousex=bx; mousey=by;
                            } else if (midbutdown) {
                            mousetransmotion(
                                event.motion.x-bx,
                                event.motion.y-by);
                                SDL_WarpMouseGlobal(bx,by);
                                mousex=bx; mousey=by;
                            }
                        }
                        break;

                    case SDL_WINDOWEVENT_RESIZED:
                        vidresize(  event.window.data1, event.window.data2 );
                        break;
                } /* switch(event.type) */
            } /* while (PollEvent) */
            } /* if (WaitEvent()) */

            FD_ZERO(&readfds);
            FD_ZERO(&writefds);
            FD_SET(masterfd,&readfds);

            tv.tv_sec=0;
            tv.tv_usec=0;
            while (0<select(masterfd+1,&readfds,&writefds,NULL,&tv)) {

                if (FD_ISSET(masterfd,&readfds)) {
                    if (1==read(masterfd,&keychar,1)) {
                        gltermWrite(term,keychar);
                    } else {
                        perror("read");
                    }
                }
                FD_ZERO(&readfds);
                FD_ZERO(&writefds);
                FD_SET(masterfd,&readfds);

                tv.tv_sec=0;
                tv.tv_usec=0;
            }

            keychar=0;

        } while(!frameflag); /* while (!frameflag) */

        timebot=SDL_GetTicks();

        frameno++;
    }
    return 0;
}   
